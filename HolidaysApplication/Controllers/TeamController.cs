﻿using System.Collections.Generic;
using AutoMapper;
using Contracts.DTOs;
using Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Database.Models;

namespace HolidaysApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService _teamService;
        private readonly IMapper _mapper;

        public TeamController(ITeamService teamService, IMapper mapper)
        {
            _teamService = teamService;
            _mapper = mapper;
        }
        // GET: api/team/GetTeams
        [HttpGet("GetTeams")]
        public IEnumerable<TeamDTO> GetTeams()
        {
            var list = _teamService.GetAll();
            var mappedList = _mapper.Map<IEnumerable<TeamDTO>>(list);
            return mappedList;
        }

        // GET: api/team/GetTeamById/id
        [HttpGet("GetTeamById/{id}")]
        public TeamDTO GetTeamById(int id)
        {
            var team = _teamService.GetById(id);
            var mappedTeam = _mapper.Map<TeamDTO>(team);
            return mappedTeam;
        }

        // GET: api/team/GetTeamByName/id
        [HttpGet("GetTeamByName/{name}")]
        public TeamDTO GetTeamByName(string name)
        {
            var team = _teamService.GetTeamByName(name);
            var mappedTeam = _mapper.Map<TeamDTO>(team);
            return mappedTeam;
        }

        // POST: api/team/postTeam
        [HttpPost("PostTeam")]
        public IActionResult PostTeam([FromBody] TeamDTO team)
        {
            var mappedTeam = _mapper.Map<Team>(team);
            _teamService.Add(mappedTeam);
            return Ok(team);
        }

        // DELETE: api/team/DeleteTeamById/id
        [HttpDelete("DeleteTeamById/{id}")]
        public void DeleteTeam(int id)
        {

            _teamService.DeleteById(id);
        }

        // PUT: api/team/UpdateTeamById/id
        [HttpPut("UpdateTeamById/{id}")]
        public void UpdateTeam(int id, [FromBody] TeamDTO team)
        {
            var mappedTeam = _mapper.Map<Team>(team);
            _teamService.Update(id, mappedTeam);
        }
    }

}
