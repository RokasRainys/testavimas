﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Database.Models;
using Infrastructure.Services;
using Contracts.DTOs;
using Infrastructure.Interfaces;
using ScheduledTasks.Services;

namespace HolidaysApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VacationController : ControllerBase
    {
        private readonly IVacationService _vacationService;
        private readonly IMapper _mapper;
        private readonly IEmailService _emailService;
        private readonly IEmployeeService _employeeService;
        private readonly ITeamService _teamService;

        public VacationController(IVacationService vacationService, IMapper mapper, IEmailService emailService, 
            IEmployeeService employeeService, ITeamService teamService)
        {
            _vacationService = vacationService;
            _mapper = mapper;
            _emailService = emailService;
            _employeeService = employeeService;
            _teamService = teamService;
        }

        // GET: api/GetAllVacations
        [HttpGet("GetAllVacations")]
        public IEnumerable<VacationDTO> GetAllVacations()
        {
            var list = _vacationService.GetVacations();
            var mappedList = _mapper.Map<IEnumerable<VacationDTO>>(list);

            return mappedList;
        }

        // POST: api/PostVacation
        [HttpPost("PostVacation")]
        public void PostVacation([FromBody] VacationDTO vacationToPost)
        {
            var vacationToAdd = _mapper.Map<Vacation>(vacationToPost);
            if (vacationToAdd != null)
            {
                _vacationService.AddVacation(vacationToAdd);
                if (_employeeService.GetEmployeeById(vacationToAdd.EmployeeId).TeamId != 0 && 
                    _teamService.GetById(_employeeService.GetEmployeeById(vacationToPost.EmployeeId).TeamId.Value) != null)
                {
                    var team = _teamService.GetById(_employeeService.GetEmployeeById(vacationToPost.EmployeeId).TeamId
                        .Value);
                    _emailService.SendVacationEmailToClient(vacationToAdd,
                        _employeeService.GetEmployeeById(vacationToAdd.EmployeeId),
                        team.ClientName, team.ClientEmail);
                }
            }
        }

        // PUT: api/ConfirmVacation/{id}
        [HttpPut("ConfirmVacation/{id}")]
        public void ConfirmVacation(int id)
        {
            _vacationService.ConfirmVacation(id);
        }

        // PUT: api/DisallowVacation/{id}
        [HttpPut("DisallowVacation/{id}")]
        public void DisallowVacation(int id)
        {
            _vacationService.DisallowVacation(id);
        }

        // PUT: api/UpdateVacation/{id}
        [HttpPut("UpdateVacation/{id}")]
        public void UpdateVacation(int id, [FromBody] VacationDTO vacation)
        {
            var mappedVacation = _mapper.Map<Vacation>(vacation);
            _vacationService.UpdateVacation(id, mappedVacation);
            var team = _teamService.GetById(_employeeService.GetEmployeeById(mappedVacation.EmployeeId).TeamId
                .Value);
            _emailService.SendVacationEmailToClient(mappedVacation,
                _employeeService.GetEmployeeById(mappedVacation.EmployeeId),
                team.ClientName, team.ClientEmail);
        }

        [HttpGet("GetVacationsByEmployee/{id}")]
        public List<VacationDTO> GetVacationsByEmployee(int id)
        {
            var vacations = _vacationService.GetVacationsByEmployeeID(id);
            return _mapper.Map<List<VacationDTO>>(vacations);
        }

        [HttpDelete("DeleteVacation/{id}")]
        public void DeleteVacation(int id)
        {
            _vacationService.DeleteVacationById(id);
        }

        [HttpGet("GetVacationsByTeamId/{id}")]
        public List<EmployeeVacationDTO> GetVacationsByTeamId(int id)
        {
            return _vacationService.GetVacationsByTeam(id);
        }
    }
}