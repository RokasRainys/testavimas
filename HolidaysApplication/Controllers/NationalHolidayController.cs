﻿using System.Collections.Generic;
using Database.Models;
using Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace HolidaysApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NationalHolidayController : ControllerBase
    {
        private readonly INationalHolidayService _nationalHolidayService;

        public NationalHolidayController(INationalHolidayService nationalHolidayService)
        {
            _nationalHolidayService = nationalHolidayService;
        }

        [HttpPost("PostNationalHoliday")]
        public void PostNationalHoliday([FromBody] NationalHoliday data)
        {
            _nationalHolidayService.AddNationalHoliday(data);
        }

        [HttpGet("GetNationalHolidays")]
        public IEnumerable<NationalHoliday> GetNationalHolidays()
        {
            return _nationalHolidayService.GetAll();
        }

        [HttpPut("UpdateNationalHoliday")]
        public void UpdateNationalHoliday(int id, [FromBody] NationalHoliday data)
        {
            _nationalHolidayService.Update(id, data);
        }

        [HttpDelete("DeleteMoveableHolidays")]
        public void DeleteMoveableHolidays()
        {
            _nationalHolidayService.DeleteMoveableHolidays();
        }

        [HttpPut("ResetHolidayDates")]
        public void ResetHolidayDates()
        {
            _nationalHolidayService.ResetHolidayYears();
        }
    }
}