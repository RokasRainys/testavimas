﻿using System.Collections.Generic;
using AutoMapper;
using Contracts.DTOs;
using Database.Models;
using Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace HolidaysApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        private readonly IMapper _mapper;


        public EmployeeController(IEmployeeService employeeService, IMapper mapper)
        {
            _employeeService = employeeService;
            _mapper = mapper;

        }

        // GET: api/EmployeeController
        [HttpGet("GetEmployees")]
        public IEnumerable<EmployeeDTO> GetEmployees()
        {
            var list = _employeeService.GetAll();
            var mappedList = _mapper.Map<IEnumerable<EmployeeDTO>>(list);
            return mappedList;
        }

        [HttpGet("GetEmployeeById/{id}")]
        public EmployeeDTO GetEmployeeById(int id)
        {
            var employee = _employeeService.GetById(id);
            var mappedEmployee = _mapper.Map<EmployeeDTO>(employee);
            return mappedEmployee;
        }

        [HttpGet("GetEmployeeByEmail/{email}")]
        public EmployeeDTO GetEmployeeByEmail(string email)
        {
            var employee = _employeeService.GetEmployeeByEmail(email);
            var mappedEmployee = _mapper.Map<EmployeeDTO>(employee);

            return mappedEmployee;
        }

        [HttpPost("PostEmployee")]
        public IActionResult PostEmployee([FromBody] EmployeeDTO employee)
        {
            if (employee.TeamId == null)
                employee.TeamId = 0;
            var mappedEmloyee = _mapper.Map<Employee>(employee);
            _employeeService.Add(mappedEmloyee, employee.Password);
            return Ok(employee);
        }

        [HttpPost("Authenticate")]
        public EmployeeDTO Authenticate(string email, string password)
        {
            var employee = _employeeService.Authenticate(email, password);
            if (employee == null)
            {
                return null;
            }
            else
            {
                return _mapper.Map<EmployeeDTO>(employee);
            }
        }

        // DELETE: api/EmployeeController
        [HttpDelete("DeleteEmployeeById/{id}")]
        public void DeleteEmployee(int id)
        {
            _employeeService.DeleteById(id);
        }

        // PUT: api/EmployeeController
        [HttpPut("UpdateEmployeeById/{id}")]
        public void UpdateEmployee(int id, [FromBody] EmployeeDTO employee)
        {
            var mappedEmloyee = _mapper.Map<Employee>(employee);
            mappedEmloyee.EmployeeId = id;
            _employeeService.Update(id, mappedEmloyee);
        }
        // PUT: api/EmployeeController
        [HttpPut("UpdatePassword")]
        public void UpdatePassword(EmployeeDTO employee, string password)
        {
            _employeeService.UpdatePassword(employee, password);
        }

        // PUT: api/EmployeeController
        [HttpPut("AddVacationDays")]
        public void AddVacationDays()
        {
            _employeeService.AddVacationDaysForEmployees();
        }
        // PUT: api/EmployeeController
        [HttpPut("RecalculateWorkingDaysValue/{id}")]
        public void RecalculateWorkingDaysValue(int id)
        {
            _employeeService.CalculateWorkingDayValue(id);
        }
    }
}