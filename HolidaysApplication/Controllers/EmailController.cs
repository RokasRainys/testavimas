﻿using System.Collections.Generic;
using Database.Models;
using Infrastructure.Interfaces;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ScheduledTasks.Services;

namespace HolidaysApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly IEmailService _emailService;
        private readonly IEmployeeService _employeeService;
        private readonly IVacationService _vacationService;
        public EmailController(IEmailService emailService, IEmployeeService employeeService, IVacationService vacationService)
        {
            _emailService = emailService;
            _employeeService = employeeService;
            _vacationService = vacationService;
        }
        // POST: api/SendBirthdayEmail
        [HttpPost("SendBirthdayEmail")]
        public void SendBirthdayReminder()
        {
            _emailService.SendBirthdayReminderEmail(_employeeService.GetUpcomingBirthdayEmployees());
        }
        // POST: api/SendVacation/Emails
        [HttpPost("SendVacationEmails")]
        public void SendVacationEmails()
        {
            var vacations = _vacationService.GetUpComingVacations();
            List<Employee> employees = new List<Employee>();
            foreach (var vacation in vacations)
                employees.Add(_employeeService.GetEmployeeById(vacation.EmployeeId));
            _emailService.SendVacationReminderEmail(employees, vacations, _employeeService.GetEmployeeEmails(), _employeeService.GetEmployeeNames());
        }

    }
}