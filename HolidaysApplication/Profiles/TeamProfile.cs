﻿using AutoMapper;
using Contracts.DTOs;
using Database.Models;

namespace Contracts.Profiles
{
    class TeamProfile : Profile 
    {
        public TeamProfile()
        {
            CreateMap<TeamDTO, Team>();
            CreateMap<Team, TeamDTO>();
        }
    }
}
