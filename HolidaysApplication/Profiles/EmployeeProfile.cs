﻿using AutoMapper;
using Contracts.DTOs;
using Database.Models;

namespace Contracts.Profiles
{
    class EmployeeProfile : Profile
    {
        public EmployeeProfile()
        {
            CreateMap<EmployeeDTO, Employee>()
                .ForMember(x => x.EmployeeId, opt => opt.Ignore());
            CreateMap<Employee, EmployeeDTO>()
                .ForMember(x => x.Password, opt => opt.Ignore());
        }
    }
}