﻿using AutoMapper;
using Contracts.DTOs;
using Database.Models;

namespace HolidaysApplication.Profiles
{
    public class VacationProfile : Profile
    {
        public VacationProfile()
        {
            CreateMap<VacationDTO, Vacation>();
            CreateMap<Vacation, VacationDTO>();
        }
    }
}
