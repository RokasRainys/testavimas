﻿using System;
using System.Net;

namespace SchedulingApp
{
    //Needs to be built and Task added to the usable PC/Server
    public class ScheduleEmails
    {
        static void Main(string[] args)
        {
            RunBirthdays();
            RunVacationDaysIncrement();
            RunVacationReminders();
        }

        private static void RunVacationDaysIncrement()
        {
            try
            {
                var request = (HttpWebRequest) WebRequest
                    .Create("https://localhost:44353/api/Employee/AddVacationDays");
                request.Method = "PUT";
                request.GetResponse();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Vacations day increment\n" + ex.Message);
                Console.ReadKey();
            }
        }
        private static void RunVacationReminders()
        {
            try
            {
                var request = (HttpWebRequest) WebRequest
                    .Create("https://localhost:44353/api/Email/SendVacationEmails");
                request.Method = "POST";
                request.GetResponse();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Vacations reminder\n" + ex.Message);
                Console.ReadKey();
            }
        }

        private static void RunBirthdays()
        {
            try
            {
                var request = (HttpWebRequest)WebRequest
                    .Create("https://localhost:44353/api/Email/SendBirthdayEmail");
                request.Method = "POST";
                request.GetResponse();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Birthday reminders\n" + ex.Message);
                //using (var fr = new StreamWriter("../../requestLog.txt", true))
                //    fr.Write(ex.Message);
                Console.ReadKey();
            }
        }
    }
}
