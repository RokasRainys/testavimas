﻿using System;
using System.Collections.Generic;
using System.Linq;
using MailKit.Net.Smtp;
using MimeKit;
using Database.Models;

namespace ScheduledTasks.Services
{
    public interface IEmailService
    {
        void SendBirthdayReminderEmail(IEnumerable<Employee> employees);

        void SendVacationReminderEmail(IEnumerable<Employee> employees, IEnumerable<Vacation> vacations,
            List<string> recipientEmails, List<string> recipientNames);

        void SendVacationEmailToClient(Vacation vacation, Employee employee, string clientName, string ClientEmail);
    }

    public class EmailService : IEmailService
    {
        public void SendBirthdayReminderEmail(IEnumerable<Employee> employees)
        {
            var sender = new MailboxAddress("Admin", "xplicitygddr@gmail.com");
            List<MailboxAddress> recipient = new List<MailboxAddress>();
            recipient.Add(new MailboxAddress("Rokas", "rainys.of.rokas6332@gmail.com"));
            string subject = "Upcoming Birthdays";
            var message = FormMessage(sender, recipient, subject);
            var bodyBuilder = AppendBodyForBirthdayEmail(employees);
            message.Body = bodyBuilder.ToMessageBody();
            AuthorizeClient(message);
        }

        public void SendVacationReminderEmail(IEnumerable<Employee> employees, IEnumerable<Vacation> vacations,
            List<string> recipientEmails, List<string> recipientNames)
        {
            var sender = new MailboxAddress("Admin", "xplicitygddr@gmail.com");
            List<MailboxAddress> recipients = new List<MailboxAddress>();
            while (recipientEmails.Any())
            {
                recipients.Add(new MailboxAddress(recipientNames.First(), recipientEmails.First()));
                recipientEmails.RemoveAt(0);
                recipientNames.RemoveAt(0);
            }
            string subject = "Upcoming Vacations";
            var message = FormMessage(sender, recipients, subject);
            var bodyBuilder = AppendBodyForVacationEmail(employees, vacations);
            message.Body = bodyBuilder.ToMessageBody();
            AuthorizeClient(message);
        }

        public void SendVacationEmailToClient(Vacation vacation, Employee employee, string clientName, string clientEmail)
        {
            var sender = new MailboxAddress("Admin", "xplicitygddr@gmail.com");
            var recipient = new List<MailboxAddress>();
            recipient.Add(new MailboxAddress(clientName, clientEmail));
            string subject = "Vacation confirmation";
            var message = FormMessage(sender, recipient, subject);
            var bodyBuilder = AppendBodyForConfirmation(employee, vacation);
            message.Body = bodyBuilder.ToMessageBody();
            AuthorizeClient(message);
        }

        private BodyBuilder AppendBodyForConfirmation(Employee employee, Vacation vacation)
        {
            BodyBuilder bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = "<p>" + employee.Name + " " + employee.Surname +
                                   " would like to go on vacation from " +
                                   vacation.DateFrom.Date + " till " + vacation.DateTo.Date +
                                   ". If you agree that he can do it, please reply to this email</p>";
            bodyBuilder.TextBody = employee.Name + " " + employee.Surname +
                                   " would like to go on vacation from " +
                                   vacation.DateFrom.Date + " till " + vacation.DateTo.Date +
                                   ". If you agree that he can do it, please reply to this email";
            return bodyBuilder;
        }
        private MimeMessage FormMessage(MailboxAddress sender, List<MailboxAddress> recipients, string subject)
        {
            MimeMessage message = new MimeMessage();
            message.From.Add(sender);
            message.To.AddRange(recipients);
            message.Subject = subject;
            return message;
        }

        private void AuthorizeClient(MimeMessage message)
        {
            SmtpClient client = new SmtpClient();
            client.Connect("smtp.gmail.com", 465, true);
            client.Authenticate("xplicitygddr", "xplicity");
            client.Send(message);
            client.Disconnect(true);
            client.Dispose();
        }
        private BodyBuilder AppendBodyForBirthdayEmail(IEnumerable<Employee> employees)
        {
            BodyBuilder bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = "<b>Upcoming birthdays</b>";
            bodyBuilder.TextBody = "Upcoming birthdays";
            if (employees.Any())
            {
                foreach (var employee in employees)
                {
                    DateTime dateThisYear = new DateTime(DateTime.Now.Year, employee.Birthday.Month, employee.Birthday.Day);

                    bodyBuilder.HtmlBody += "<p>" + employee.Name + " " + employee.Surname + " on " +
                                            dateThisYear.DayOfWeek + "</p>";
                    bodyBuilder.TextBody +=
                        employee.Name + " " + employee.Surname + " on " + dateThisYear.DayOfWeek;
                }
            }
            else
            {
                bodyBuilder.HtmlBody = "<p>There are no upcoming birthdays<p>";
                bodyBuilder.TextBody = "There are no upcoming birthdays";
            }
            return bodyBuilder;
        }

        private BodyBuilder AppendBodyForVacationEmail(IEnumerable<Employee> employees,
            IEnumerable<Vacation> vacations)
        {
            BodyBuilder bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = "<b>Upcoming vacations</b>";
            bodyBuilder.TextBody = "Upcoming vacations";
            if (vacations.Any())
            {
                foreach (var vacation in vacations)
                {
                    var employee = employees.FirstOrDefault(x => x.EmployeeId == vacation.EmployeeId);
                    bodyBuilder.HtmlBody += "<p>" + employee.Name + " " + employee.Surname + " goes on vacation from " +
                                            vacation.DateFrom.Date + " till " + vacation.DateTo.Date + "</p>";
                    bodyBuilder.TextBody += employee.Name + " " + employee.Surname + " goes on vacation from " +
                                            vacation.DateFrom.Date + " till " + vacation.DateTo.Date;
                }
            }
            else
            {
                bodyBuilder.HtmlBody = "<p>No vacations</p>";
                bodyBuilder.TextBody = "No vacations";
            }
            return bodyBuilder;
        }
    }
}
