using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public int? TeamId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public bool IsAdmin { get; set; }

        [Required]
        [Column(TypeName = "date")]
        public DateTime EmploymentDate { get; set; }

        [Required]
        [Column(TypeName = "date")]
        public DateTime Birthday { get; set; }

        [Required]
        public bool HasChildrenUnder14 { get; set; }
        public float VacationPerDay { get; set; }
        public float VacationAmount { get; set; }
        public byte[] passwordSalt { get; set; }
        public byte[] passwordHash { get; set; }
    }
}