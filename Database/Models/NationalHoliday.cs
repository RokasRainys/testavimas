﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Database.Models
{
    public class NationalHoliday
    {
        public int NationalHolidayId { get; set; }
        [Required]
        public bool DoesChange { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
