﻿namespace Database.enums
{
    public enum VacationType
    {
        Paid,
        Paternity,
        Science,
    }
}
