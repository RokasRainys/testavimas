﻿using System.Linq;
using Database;
using Database.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Repositories
{
    public class TeamRepository : Repository<Team>, ITeamRepository
    {
        public TeamRepository(HolidaysDbContext context) : base(context)
        {
        }

        public Team ReadByName(string name)
        {
            var team = (from b in Context.Teams
                where (b.Name == name)
                select b).FirstOrDefault();

            return team;
        }
    }
}