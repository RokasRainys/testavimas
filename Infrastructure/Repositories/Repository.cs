﻿using System.Collections.Generic;
using System.Linq;
using Database;
using Infrastructure.Interfaces;

namespace Infrastructure.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly HolidaysDbContext Context;

        public Repository(HolidaysDbContext context)
        {
            Context = context;
        }

        public void Create(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);
            Context.SaveChanges();
        }

        public TEntity Read(int id)
        {
            return Context.Set<TEntity>().Find(id);
        }

        public void Update(TEntity entity, int id)
        {
            var oldData = Context.Set<TEntity>().Find(id);
                        if(oldData != null)
                        {
                            Context.Entry(oldData).CurrentValues.SetValues(entity);
                            Context.SaveChanges();
                        }

        }

        public void Delete(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
            Context.SaveChanges();
        }

        public IEnumerable<TEntity> ReadAll()
        {
            return Context.Set<TEntity>().ToList();
        }
    }
}