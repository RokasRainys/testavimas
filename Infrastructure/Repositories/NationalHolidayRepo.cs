﻿using System;
using System.Linq;
using Database;
using Database.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Repositories
{
    public class NationalHolidayRepository : Repository<NationalHoliday>, INationalHolidayRepository
    {
        public NationalHolidayRepository(HolidaysDbContext context) : base(context)
        {
        }

        public NationalHoliday ReadByDate(DateTime date)
        {
            var holiday = (from b in Context.NationalHolidays
                where b.Date.Date == date
                           select b).FirstOrDefault();
            return holiday;
        }

        public void ResetNotMovingHolidays()
        {
            var notMovingHolidays = (from b in Context.NationalHolidays
                where b.DoesChange == false
                select b).ToList();
            notMovingHolidays.All(x =>
            {
                x.Date = x.Date.AddYears(1);
                return true;
            });
            Context.Set<NationalHoliday>().UpdateRange(notMovingHolidays);
            Context.SaveChanges();
        }
        public void DeleteMoveableHolidays()
        {
            var moveableHolidays = (from b in Context.NationalHolidays
                where b.DoesChange == true
                select b).ToList();
            Context.Set<NationalHoliday>().RemoveRange(moveableHolidays);
            Context.SaveChanges();
        }
    }
}
