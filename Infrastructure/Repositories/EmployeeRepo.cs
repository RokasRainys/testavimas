﻿using System;
using System.Collections.Generic;
using System.Linq;
using Database;
using Database.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Repositories
{
    public class EmployeeRepository : Repository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(HolidaysDbContext context) : base(context)
        {
        }

        public Employee Read(string email)
        {
            var employee = (from b in Context.Employees
                where (b.Email == email)
                select b).FirstOrDefault(); 

            return employee;
        }

        public List<Employee> ReadByTeamId(int id)
        {
            return (from b in Context.Employees
                where (b.TeamId == id)
                select b).ToList();
        }
        public List<string> ReadAllEmails()
        {
            return (from b in Context.Employees
                select b.Email).ToList();
        }

        public List<string> ReadAllNames()
        {
            return (from b in Context.Employees
                select b.Name).ToList();
        }

        public List<Employee> GetUpcomingBirthdayEmployees(int daysUpfront)
        {
            return (Context.Employees.Where(b => (b.Birthday.Date.Month == DateTime.Now.Month &&
                                                  b.Birthday.Date.Day == DateTime.Now.AddDays(daysUpfront).Day))).ToList();
        }
    }
}