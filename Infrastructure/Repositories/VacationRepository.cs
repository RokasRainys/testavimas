﻿using System;
using System.Collections.Generic;
using Infrastructure.Interfaces;
using Database;
using Database.Models;
using System.Linq;
using Contracts.DTOs;
using Database.enums;
using Remotion.Linq.Clauses;

namespace Infrastructure.Repositories
{
    public class VacationRepository : Repository<Vacation>, IVacationRepository
    {
        public VacationRepository(HolidaysDbContext context) : base(context)
        {
        }

        public Vacation ReadByEmployee(int employeeID)
        {
            var vacation = (from b in Context.Vacations
                where (b.EmployeeId == employeeID && b.DateFrom < DateTime.Now)
                            select b).FirstOrDefault();
            return vacation;
        }

        public List<Vacation> ReadListByEmployee(int employeeID)
        {
            var vacation = (from b in Context.Vacations
                where (b.EmployeeId == employeeID)
                select b).ToList();
            return vacation;
        }

        public List<Vacation> GetUpcomingVacations(int daysUpFront)
        {
            return (from b in Context.Vacations
                where (b.DateFrom.Date == DateTime.Now.AddDays(daysUpFront).Date)
                select b).ToList();
        }

        public bool IsOnUnpaidVacation(int employeeID)
        {
            var employeeVacation = (from b in Context.Vacations
                where (b.EmployeeId == employeeID &&
                       b.DateFrom.Date < DateTime.Now &&
                       b.DateTo.Date > DateTime.Now &&
                       b.VacationType != VacationType.Science)
                select b).FirstOrDefault();
            if (employeeVacation != null)
                return true;
            return false;
        }

        public List<Vacation> GetVacationsByEmployeeID(int employeeID)
        {
            return (from b in Context.Vacations
                where (b.EmployeeId == employeeID)
                select b).ToList();
        }
    }
}
