﻿using System.Collections.Generic;

namespace Infrastructure.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Create(TEntity data);
        TEntity Read(int id);
        void Update(TEntity data, int id);
        void Delete(TEntity data);
        IEnumerable<TEntity> ReadAll();
    }
}
