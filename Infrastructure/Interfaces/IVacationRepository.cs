﻿using System.Collections.Generic;
using Database.Models;

namespace Infrastructure.Interfaces
{
    public interface IVacationRepository : IRepository<Vacation>
    {
        Vacation ReadByEmployee(int employeeID);

        List<Vacation> GetUpcomingVacations(int daysUpFront);
        bool IsOnUnpaidVacation(int employeeID);
        List<Vacation> GetVacationsByEmployeeID(int employeeID);

        List<Vacation> ReadListByEmployee(int employeeID);
    }
}
