﻿using System.Collections.Generic;
using System.Reflection.PortableExecutable;

namespace Infrastructure.Interfaces
{
    public interface IService<TEntity> where TEntity : class
    {
        void Update(int id, TEntity data);
        void Delete(TEntity data);
        void DeleteById(int id);
        TEntity GetById(int id);
        IEnumerable<TEntity> GetAll();
    }
}
