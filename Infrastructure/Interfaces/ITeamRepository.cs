﻿using Database.Models;

namespace Infrastructure.Interfaces
{
    public interface ITeamRepository : IRepository<Team>
    {
        Team ReadByName(string name);
    }
}
