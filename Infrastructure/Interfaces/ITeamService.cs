﻿using Database.Models;

namespace Infrastructure.Interfaces
{
    public interface ITeamService : IService<Team>
    {
        void Add(Team teamToAdd);
        Team GetTeamByName(string name);
        bool ValidateByName(Team teamToValidate);
    }
}
