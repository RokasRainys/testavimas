﻿using System.Collections.Generic;
using Database.Models;

namespace Infrastructure.Interfaces
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        Employee Read(string email);

        List<string> ReadAllEmails();

        List<string> ReadAllNames();

        List<Employee> GetUpcomingBirthdayEmployees(int daysUpfront);
        List<Employee> ReadByTeamId(int id);
    }
}
