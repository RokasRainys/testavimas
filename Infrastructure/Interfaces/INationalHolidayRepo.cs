﻿using System;
using Database.Models;

namespace Infrastructure.Interfaces
{
    public interface INationalHolidayRepository : IRepository<NationalHoliday>
    {
        NationalHoliday ReadByDate(DateTime date);
        void DeleteMoveableHolidays();
        void ResetNotMovingHolidays();
    }
}
