﻿using Database.Models;

namespace Infrastructure.Interfaces
{
    public interface INationalHolidayService : IService<NationalHoliday>
    {
        void AddNationalHoliday(NationalHoliday data);
        bool Validate(NationalHoliday dataToValidate);
        void DeleteMoveableHolidays();

        void ResetHolidayYears();
    }
}
