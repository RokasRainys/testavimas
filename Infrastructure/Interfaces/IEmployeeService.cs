﻿using System.Collections.Generic;
using Contracts.DTOs;
using Database.Models;

namespace Infrastructure.Interfaces
{
    public interface IEmployeeService : IService<Employee>
    {
        void Add(Employee employeeToAdd, string password);
        Employee GetEmployeeByEmail(string email);
        bool ValidateByEmail(Employee employeeToValidate);
        IEnumerable<Employee> GetUpcomingBirthdayEmployees();
        Employee GetEmployeeById(int id);
        List<string> GetEmployeeNames();
        List<string> GetEmployeeEmails();
        void CalculateWorkingDayValue(int id);
        void AddVacationDaysForEmployees();
        Employee Authenticate(string email, string password);
        bool UpdatePassword(EmployeeDTO employee, string newPassword);
    }

}
