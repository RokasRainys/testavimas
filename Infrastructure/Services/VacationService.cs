﻿using System;
using System.Collections.Generic;
using Contracts.DTOs;
using Database.enums;
using Infrastructure.Interfaces;
using Database.Models;

namespace Infrastructure.Services
{
    public interface IVacationService
    {
        void AddVacation(Vacation vacationToAdd);
        void UpdateVacation(int id, Vacation vacationToUpdate);
        void DeleteVacationById(int id);
        void ConfirmVacation(int id);
        void DisallowVacation(int id);
        IEnumerable<Vacation> GetVacations();
        IEnumerable<Vacation> GetUpComingVacations();
        bool Validate(Vacation vacationToValidate);
        List<Vacation> GetVacationsByEmployeeID(int employeeID);
        List<EmployeeVacationDTO> GetVacationsByTeam(int id);
    }
    public class VacationService : IVacationService
    {
        private readonly IVacationRepository _vacationRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ITeamRepository _teamRepository;

        public VacationService(IVacationRepository vacationRepository, IEmployeeRepository employeeRepository,
            ITeamRepository teamRepository)
        {
            _vacationRepository = vacationRepository;
            _employeeRepository = employeeRepository;
            _teamRepository = teamRepository;
        }

        public void AddVacation(Vacation vacationToAdd)
        {
            if (Validate(vacationToAdd))
            {
                if (vacationToAdd.VacationType == VacationType.Paid)
                {
                    var employee = _employeeRepository.Read(vacationToAdd.EmployeeId);
                    employee.VacationAmount -= CalculateVacationDays(vacationToAdd);
                    _employeeRepository.Update(employee, vacationToAdd.EmployeeId);
                }
                _vacationRepository.Create(vacationToAdd);
            }
        }

        public void UpdateVacation(int id, Vacation vacationToUpdate)
        {
            if (vacationToUpdate.VacationType == VacationType.Paid)
            {
                if (CalculateVacationDays(vacationToUpdate) != CalculateVacationDays(_vacationRepository.Read(id)))
                {
                    var employee = _employeeRepository.Read(vacationToUpdate.EmployeeId);
                    employee.VacationAmount -= CalculateVacationDays(vacationToUpdate) -
                                               CalculateVacationDays(_vacationRepository.Read(id));
                    _employeeRepository.Update(employee, vacationToUpdate.EmployeeId);
                    _vacationRepository.Update(vacationToUpdate, id);
                }
                else
                {
                    _vacationRepository.Update(vacationToUpdate, id);
                }
            }
            else
            {
                _vacationRepository.Update(vacationToUpdate, id);
            }
        }

        public void DeleteVacationById(int id)
        {
            var vacationToDelete = _vacationRepository.Read(id);
            if (vacationToDelete.VacationType == VacationType.Paid)
            {
                var employee = _employeeRepository.Read(vacationToDelete.EmployeeId);
                employee.VacationAmount += CalculateVacationDays(vacationToDelete);
                _employeeRepository.Update(employee, vacationToDelete.EmployeeId);
            }
            _vacationRepository.Delete(vacationToDelete);
        }

        public void ConfirmVacation(int id)
        {
            var vacation = _vacationRepository.Read(id);
            if (vacation != null)
            {
                var employee = _employeeRepository.Read(vacation.EmployeeId);
                vacation.IsConfirmed = true;
                _vacationRepository.Update(vacation, id);
            }
        }

        public void DisallowVacation(int id)
        {
            var vacation = _vacationRepository.Read(id);
            if (vacation != null)
            {
                var employee = _employeeRepository.Read(vacation.EmployeeId);
                vacation.IsConfirmed = false;
                _vacationRepository.Update(vacation, id);
            }
        }

        public IEnumerable<Vacation> GetVacations()
        {
            return _vacationRepository.ReadAll();
        }

        public IEnumerable<Vacation> GetUpComingVacations()
        {
            var today = DateTime.Now;
            List<Vacation> vacations = new List<Vacation>();
            if (today.DayOfWeek == DayOfWeek.Friday)
            {
                vacations = _vacationRepository.GetUpcomingVacations(3);
            }
            else if (today.DayOfWeek != DayOfWeek.Saturday && today.DayOfWeek != DayOfWeek.Sunday)
            {
                vacations.AddRange(_vacationRepository.GetUpcomingVacations(1));
            }
            return vacations;
        }

        public bool Validate(Vacation vacationToValidate)
        {
            var employee = _employeeRepository.Read(vacationToValidate.EmployeeId);
            if (CalculateVacationDays(vacationToValidate) > employee.VacationAmount)
                return false;
            return true;
        }

        public List<Vacation> GetVacationsByEmployeeID(int employeeID)
        {
            return _vacationRepository.GetVacationsByEmployeeID(employeeID);
        }

        public List<EmployeeVacationDTO> GetVacationsByTeam(int id)
        {
            var teamEmployees = _employeeRepository.ReadByTeamId(id);
            List<EmployeeVacationDTO> employeeVacationList = new List<EmployeeVacationDTO>();
            foreach (var employee in teamEmployees)
            {
                var employeeVacations = _vacationRepository.ReadListByEmployee(employee.EmployeeId);
                foreach (var vacation in employeeVacations)
                {
                    EmployeeVacationDTO dto = new EmployeeVacationDTO();
                    dto.EmployeeName = employee.Name;
                    dto.EmployeeSurname = employee.Surname;
                    dto.VacationDateFrom = vacation.DateFrom;
                    dto.VacationDateTill = vacation.DateTo;
                    employeeVacationList.Add(dto);
                }
            }
            return employeeVacationList;
        }

        private int CalculateVacationDays(Vacation vacation)
        {
            int vacationDays = 0;
            for (var day = vacation.DateFrom; day <= vacation.DateTo; day = day.AddDays(1))
            {
                if (day.DayOfWeek != DayOfWeek.Saturday && day.DayOfWeek != DayOfWeek.Sunday)
                    vacationDays++;
            }
            return vacationDays;
        }
    }
}
