﻿using Database.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Services
{
    public class TeamService : Service<Team>, ITeamService
    {
        private readonly ITeamRepository _teamRepository;

        public TeamService(ITeamRepository teamRepository,
            IRepository<Team> repository) : base(repository)
        {
            _teamRepository = teamRepository;
        }

        public void Add(Team teamToAdd)
        {
            if (ValidateByName(teamToAdd))
                _teamRepository.Create(teamToAdd);
        }

        public Team GetTeamByName(string name)
        {
            return _teamRepository.ReadByName(name);
        }

        public bool ValidateByName(Team employeeToValidate)
        {
            return _teamRepository.ReadByName(employeeToValidate.Name) == null;
        }
    }
}