﻿using Database.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Services
{
    public class NationalHolidayService : Service<NationalHoliday>, INationalHolidayService
    {
        private readonly INationalHolidayRepository _nationalHolidayRepository;

        public NationalHolidayService(INationalHolidayRepository nationalHolidayRepository) : base(nationalHolidayRepository)
        {
            _nationalHolidayRepository = nationalHolidayRepository;
        }

        public void AddNationalHoliday(NationalHoliday data)
        {
            if (Validate(data))
                _nationalHolidayRepository.Create(data);
        }

        public bool Validate(NationalHoliday dataToValidate)
        {
            if (_nationalHolidayRepository.ReadByDate(dataToValidate.Date) == null)
                return true;
            return false;
        }

        public void DeleteMoveableHolidays()
        {
            _nationalHolidayRepository.DeleteMoveableHolidays();
        }

        public void ResetHolidayYears()
        {
            _nationalHolidayRepository.ResetNotMovingHolidays();
        }
    }
}
