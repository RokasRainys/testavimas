﻿using System.Collections.Generic;
using AutoMapper;
using Infrastructure.Interfaces;

namespace Infrastructure.Services
{
    public class Service<TEntity> : IService<TEntity> where TEntity : class
    {
        private readonly IRepository<TEntity> _repository;

        public Service(IRepository<TEntity> repository)
        {
            _repository = repository;
        }

        public void Update(int id, TEntity data)
        {
            var temp = _repository.Read(id);
            if (temp != null)
                _repository.Update(data, id);
        }

        public void Delete(TEntity data)
        {
            _repository.Delete(data);
        }

        public void DeleteById(int id)
        {
            var temp = _repository.Read(id);
            if (temp != null)
                Delete(temp);
        }

        public TEntity GetById(int id)
        {
            return _repository.Read(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _repository.ReadAll();
        }
    }
}
