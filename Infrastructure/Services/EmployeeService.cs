﻿using System;
using System.Collections.Generic;
using Contracts.DTOs;
using Database.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Services
{
    public class EmployeeService : Service<Employee>, IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly INationalHolidayRepository _nationalHolidayRepository;
        private readonly IVacationRepository _vacationRepository;

        public EmployeeService(IEmployeeRepository employeeRepository,
            IRepository<Employee> repository, INationalHolidayRepository nationalHolidayRepository,
            IVacationRepository vacationRepository) : base(repository)
        {
            _employeeRepository = employeeRepository;
            _nationalHolidayRepository = nationalHolidayRepository;
            _vacationRepository = vacationRepository;
        }

        public void Add(Employee employeeToAdd, string password)
        {
            if (ValidateByEmail(employeeToAdd))
            {
                CreatePasswordHash(password, out var passwordHash, out var passwordSalt);
                employeeToAdd.passwordHash = passwordHash;
                employeeToAdd.passwordSalt = passwordSalt;
                int maxVacationDays = 20;
                if (employeeToAdd.HasChildrenUnder14)
                {
                    maxVacationDays = 25;
                }
                var workingDays = 0;
                if ((int) CalculateTimeDifference(employeeToAdd.EmploymentDate.Date, DateTime.Now) / 365.25 > 0)
                {
                    var dateFrom = employeeToAdd.EmploymentDate.AddYears(
                        (int) (CalculateTimeDifference(employeeToAdd.EmploymentDate.Date, DateTime.Now) / 365.25));
                    workingDays = CalculateWorkingDaysPerYear(dateFrom);
                    employeeToAdd.VacationPerDay = (float) maxVacationDays / workingDays;
                    var daysDifference = CalculateWorkingDaysDifference(dateFrom);
                    employeeToAdd.VacationAmount = daysDifference * employeeToAdd.VacationPerDay;
                }
                else
                {
                    workingDays = CalculateWorkingDaysPerYear(employeeToAdd.EmploymentDate);
                    employeeToAdd.VacationPerDay = (float)maxVacationDays / workingDays;
                    if (DateTime.Now >= employeeToAdd.EmploymentDate.Date)
                    {
                        var daysDifference = CalculateWorkingDaysDifference(employeeToAdd.EmploymentDate);
                        employeeToAdd.VacationAmount = daysDifference * employeeToAdd.VacationPerDay;
                    }
                }
                _employeeRepository.Create(employeeToAdd);
            }
        }
        public Employee GetEmployeeByEmail(string email)
        {
            return _employeeRepository.Read(email);
        }
        public List<string> GetEmployeeEmails()
        {
            return _employeeRepository.ReadAllEmails();
        }
        public void CalculateWorkingDayValue(int id)
        {
            var employee = _employeeRepository.Read(id);
            int maxVacationDays = 20;
            int workingDays;
            if (employee.HasChildrenUnder14)
            {
                maxVacationDays = 25;
            }
            if ((int)CalculateTimeDifference(employee.EmploymentDate.Date, DateTime.Now) / 365.25 > 0)
            {
                var dateFrom = employee.EmploymentDate.AddYears(
                    (int)(CalculateTimeDifference(employee.EmploymentDate.Date, DateTime.Now) / 365.25));
                workingDays = CalculateWorkingDaysPerYear(dateFrom);
                employee.VacationPerDay = (float)maxVacationDays / workingDays;
                int daysDifference = CalculateWorkingDaysDifference(dateFrom);
                employee.VacationAmount = daysDifference * employee.VacationPerDay;
            }
            else
            {
                workingDays = CalculateWorkingDaysPerYear(employee.EmploymentDate);
                employee.VacationPerDay = (float)maxVacationDays / workingDays;
                if (DateTime.Now.Date >= employee.EmploymentDate.Date)
                {
                    int daysDifference = CalculateWorkingDaysDifference(employee.EmploymentDate);
                    employee.VacationAmount = daysDifference * employee.VacationPerDay;
                }
            }

            _employeeRepository.Update(employee, employee.EmployeeId);
        }
        public List<string> GetEmployeeNames()
        {
            return _employeeRepository.ReadAllNames();
        }
        public IEnumerable<Employee> GetUpcomingBirthdayEmployees()
        {
            var today = DateTime.Now;
            List<Employee> upcomingBirthdayEmployees = new List<Employee>();
            if (today.DayOfWeek == DayOfWeek.Thursday)
            {
                upcomingBirthdayEmployees = _employeeRepository.GetUpcomingBirthdayEmployees(1);
                upcomingBirthdayEmployees.AddRange(_employeeRepository.GetUpcomingBirthdayEmployees(2));
                upcomingBirthdayEmployees.AddRange(_employeeRepository.GetUpcomingBirthdayEmployees(3));
            }
            else if (today.DayOfWeek == DayOfWeek.Friday)
                upcomingBirthdayEmployees = _employeeRepository.GetUpcomingBirthdayEmployees(3);
            else if (today.DayOfWeek != DayOfWeek.Friday && today.DayOfWeek != DayOfWeek.Saturday && today.DayOfWeek != DayOfWeek.Sunday)
                upcomingBirthdayEmployees = _employeeRepository.GetUpcomingBirthdayEmployees(1);
            return upcomingBirthdayEmployees;
        }
        public Employee GetEmployeeById(int id)
        {
            return _employeeRepository.Read(id);
        }
        public void AddVacationDaysForEmployees()
        {
            var employees = _employeeRepository.ReadAll();
            foreach (var employee in employees)
            {
                if (employee.EmploymentDate.Date < DateTime.Now.Date)
                {
                    if (!_vacationRepository.IsOnUnpaidVacation(employee.EmployeeId))
                    {
                        if (employee.HasChildrenUnder14 && employee.VacationAmount < 25)
                        {
                            employee.VacationAmount += employee.VacationPerDay;
                        }
                        else if (!employee.HasChildrenUnder14 && employee.VacationAmount < 20)
                        {
                            employee.VacationAmount += employee.VacationPerDay;
                        }
                    }

                    _employeeRepository.Update(employee, employee.EmployeeId);
                }
            }
        }
        public Employee Authenticate(string email, string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
            {
                return null;
            }
            var employee = GetEmployeeByEmail(email);
            if (employee == null)
            {
                return null;
            }
            if (!VerifyPasswordHash(password, employee.passwordHash, employee.passwordSalt))
            {
                return null;
            }
            return employee;
        }

        public bool UpdatePassword(EmployeeDTO employee, string newPassword)
        {
            var oldEmployee = _employeeRepository.Read(employee.EmployeeId);
            if (string.IsNullOrEmpty(newPassword))
            {
                return false;
            }

            if (!VerifyPasswordHash(employee.Password, oldEmployee.passwordHash, oldEmployee.passwordSalt))
            {
                return false;
            }

            CreatePasswordHash(newPassword, out var passwordHash, out var passwordSalt);
            oldEmployee.passwordHash = passwordHash;
            oldEmployee.passwordSalt = passwordSalt;
            _employeeRepository.Update(oldEmployee, oldEmployee.EmployeeId);
            return true;
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }
        public bool ValidateByEmail(Employee employeeToValidate)
        {
            return _employeeRepository.Read(employeeToValidate.Email) == null;
        }
        private int CalculateTimeDifference(DateTime dateFrom, DateTime dateNow)
        {
            if (dateFrom.Date > dateNow.Date)
            {
                return 0;
            }
            TimeSpan timeSpan = dateNow.Date - dateFrom.Date;
            return timeSpan.Days;
        }

        private int CalculateWorkingDaysDifference(DateTime dateFrom)
        {
            int days = 0;
            while (dateFrom.Date != DateTime.Now.Date)
            {
                if (dateFrom.DayOfWeek != DayOfWeek.Saturday && dateFrom.DayOfWeek != DayOfWeek.Sunday
                                                             && _nationalHolidayRepository.ReadByDate(dateFrom.Date) ==
                                                             null)
                {
                    days++;
                }

                dateFrom = dateFrom.AddDays(1);
            }

            return days;
        }
        private int CalculateWorkingDaysPerYear(DateTime dateFrom)
        {
            int days = 0;
            var dateAfterOneYear = dateFrom.AddYears(1);
            while (dateFrom.Date != dateAfterOneYear.Date)
            {
                if (dateFrom.DayOfWeek != DayOfWeek.Saturday && dateFrom.DayOfWeek != DayOfWeek.Sunday
                                                             && _nationalHolidayRepository.ReadByDate(dateFrom.Date) == null)
                {
                    days++;
                }
                dateFrom = dateFrom.AddDays(1);
            }
            return days;
        }
    }
}
